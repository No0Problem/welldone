﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WellDone.Models
{
    public class PostMapping : IEntityTypeConfiguration<Post>
    {
        private const int MaxSize = 1500;
        public void Configure(EntityTypeBuilder<Post> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Title)
                   .HasMaxLength(MaxSize);
            builder.Property(x => x.Information)
                   .HasMaxLength(MaxSize);
            builder.Property(x => x.ProjectId)
                   .HasMaxLength(MaxSize);
        }
    }
}
