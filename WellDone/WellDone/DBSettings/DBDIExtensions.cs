﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WellDone.Models
{
    public static class DBDIExtensions
    {
        private const string DBConnectionString = nameof(DBConnectionString);

        public static void RegisterDB(this IServiceCollection collection, IConfiguration configuration)
        {
            var connection = configuration.GetConnectionString(DBConnectionString);
            collection.AddDbContextPool<ApplicationContext>(builder => builder.UseMySql(connection));
        }
    }
}
