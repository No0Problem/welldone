﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WellDone.Models
{
    public class Post
    {
        public Guid Id { get; set;}

        public string Title { get; set;}

        public string Information { get; set;}
        
        public Guid ProjectId { get; set;}
        
    }
}
